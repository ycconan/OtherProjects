#include<iostream>
#include<string>
#include<string.h>
using namespace std;

class person
{
  private:
    int age;
    char *str;
  public:
    person(int a)//普通构造函数定义
    {
        cout <<"int a"<< endl;
        this->age=a;
    }
    /*初始化，初始化列表在构造函数执行前执行(这个可以测试，对同一个变量在初始化列表和
    构造函数中分别初始化，首先执行参数列表，后在函数体内赋值，后者会覆盖前者)*/
    person() : str(NULL) { cout << "str(NULL)" << endl; }
    const char * c_str();
    inline int show();
    //常引用声明方式：const 类型标识符 &引用名=目标变量名
    inline bool operator==(const person &ps) const;
    inline char *operator=(const char *s);
    inline person &operator=(const person &as);
    inline friend person &operator+(const person &s1, const person &s2);

    // template <class T>//函数模版
    // inline T operator=(const T s);
};

inline bool person::operator==(const person &ps) const
{
    cout << "bool" << endl;
    if (this->age == ps.age)
        return true;
    return false;
}

inline int person::show()
{
    age = 12;
    return age;
}
const char *person::c_str()
{
    cout << "c_str" << endl;
    return str;
}
person s3;
inline person &operator+(const person &s1, const person &s2)
{
    cout << "相加" << endl;
    s3.str = new char[strlen(s1.str) + strlen(s2.str) + 1];
    strcpy(s3.str, s1.str);
    strcat(s3.str, s2.str);
    return s3;//当以引用的方式返回函数值时，不能为临时变量或局部变量，可以为全局变量
}
//template <class T>
// inline T person::operator=(const T s)
// {
//     cout << "赋值" << endl;
//     if(str)
//     delete [] str;
//     if(s)
//     {
//         str=new char[strlen(s)+1];
//         strcpy(str,s);
//     }
//     else
//     str=NULL;
//     return str;
// }

inline char * person::operator = (const char *s)
{
    cout << "赋值字符串" << endl;
    if(str)
    delete [] str;
    if(s)
    {
        str=new char[strlen(s)+1];
        strcpy(str,s);
    }
    else
    str=NULL;
    return str;
}

inline person & person::operator=(const person &as)
{
    cout << "赋值对象" << endl;
    if (str == as.str)
        return *this;
    if (str)
        delete[] str;
    if (as.str)
    {
        str = new char[strlen(as.str) + 1];
        strcpy(str, as.str);
     }
     else
    str=NULL;

    return *this;
}
int main()
{
    person p1(20);//实参为整数 
    person p2(10);
    if (p1 == p2)
        cout << "the age is equal!" << endl;
    else
        cout << "the age is no equal!" << endl;

    person s1,s2,s3,s4;
    s1=">hello";
    cout << s1.c_str() << endl;
    s2="  world!<";
    cout << s2.c_str() << endl;
    s3=s1;/*对象的直接赋值，如果用户没有定义复制构造函数，则系统会自动提供
        一个复制构造函数，其作用是简单的复制类中的每个人数据成员*/
    cout << s3.show() << endl;
    cout << s3.c_str() << endl;
    s4=s1+s2;
    cout << s4.c_str() << endl;
    cout << s4.show() << endl;
    return 0;
}
