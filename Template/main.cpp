#include "Example.h"
#include <iostream>
using namespace ::yuchao;
using namespace std;

int main()
{
    Example<int> val1(1);// 类模板实例化
    int data = val1.get();
    cout << "data = " << data << endl;

    Example<float> val2(3.2);
    float value = val2.get();
    cout << "value = " << value << endl;

    Test test;
    Example<Test> val3(test);
    int num = val3.output();
    cout << "num = " << num << endl;
}