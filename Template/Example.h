#ifndef _EXAMPLE_H_
#define _EXAMPLE_H_
/**命名空间**/

namespace yuchao 
{
class Test
{

public:
  Test() {}
  int output()
  {
    return 0;
  }

  virtual int other()
  {
    return 0;
  };

public:
  // Test(const Test &);
  //Test &operator=(const Test &);

private:
};
/**类模版**/
template <typename T>
class Example : public Test
{
  public:
    Example(T val) : m_val(val) {} //初始化参数
    ~Example() {}

    void set(T val)
    {
      m_val = val;
    }

    T get() const
    {
      return m_val;
    }

    int other()
    {

       return 4;
    }

  private:
    T m_val;
};

 
}
#endif