#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/select.h>
#include<time.h>

#define SER_PORT	7570
#define SER_IP		"192.168.0.193"

int server_sockfd;//服务器端套接字  
int client_sockfd;//客户端套接字  
int len;  
struct sockaddr_in my_addr;   //服务器网络地址结构体  
struct sockaddr_in remote_addr; //客户端网络地址结构体  
int sin_size;  
char buf[12]={0};  //数据传送的缓冲区 
 
void *func1(void *arg)
{
	len=recv(client_sockfd,buf,12,0);//接受信息 
	printf("bufsize=%d\n",sizeof(buf));  
	printf("received:%s\n",buf);  
	memset(buf, 0, sizeof(buf));
}  
void *func2(void *arg)
{
	printf("Enter string to send:\n");  
	scanf("%s",buf);  
	len=send(client_sockfd,buf,strlen(buf),0);  
}  
int main(int argc, char *argv[])  
{  
	pthread_t tid1,tid2;
    
    memset(&my_addr,0,sizeof(my_addr)); //数据初始化--清零  
    my_addr.sin_family=AF_INET; //设置为IP通信  
    my_addr.sin_addr.s_addr = inet_addr(SER_IP);//服务器IP地址--允许连接到所有本地地址上  
    my_addr.sin_port=htons(SER_PORT); //服务器端口号  

    /*创建服务器端套接字--IPv4协议，面向连接通信，TCP协议*/  
    if((server_sockfd=socket(PF_INET,SOCK_STREAM,0))<0)  
    {    
        perror("socket error");  
        return 1;  
    }  
	  // 设置端口运行重复绑定
	int on = 1; 
	int ret = -1;
	ret = setsockopt( server_sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)); 
  
    /*将套接字绑定到服务器的网络地址上*/  
    if(bind(server_sockfd,(struct sockaddr *)&my_addr,sizeof(struct sockaddr))<0)  
    {  
        perror("bind error");  
        return 1;  
    }  
      
    /*监听连接请求--监听队列长度为5*/  
    if(listen(server_sockfd,5)<0)  
    {  
        perror("listen error");  
        return 1;  
    };  
      
    sin_size=sizeof(struct sockaddr_in);  
      
    /*等待客户端连接请求到达*/  
    if((client_sockfd=accept(server_sockfd,(struct sockaddr *)&remote_addr,&sin_size))<0)  
    {  
        perror("accept error");  
        return 1;  
    }  
    printf("accept client %s\n",inet_ntoa(remote_addr.sin_addr));  
    
    /*接收客户端的数据并将其发送给客户端--recv返回接收到的字节数，send返回发送的字节数*/  
    while(1) 
    { 

		pthread_create(&tid1,NULL,func1,NULL);//创建线程
		pthread_create(&tid2,NULL,func2,NULL);//创建线程
		pthread_join(tid2,NULL);//接合指定线程（先执行线程）
    }  
  
  
    /*关闭套接字*/  
    close(client_sockfd);  
    close(server_sockfd);  
      
    return 0;  
}  
