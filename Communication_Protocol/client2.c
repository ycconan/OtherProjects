/**********************封包协议************************
* 													  *
* ID		 包总长度	     其他数据                 * 
*													  *	
* 00 06      00 08           00 00 00 00              *
* 2字节	     2字节	         4字节（可以为其他字节数  *
*													  *		
*包总长度 = ID字节数+包总长度字节数+其他数据字节数    *
******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/select.h>
#include<time.h> 

#define PORT     7670
#define ID_VALUE 0x0456
//#define BigLlitteSwap16(A) ((A>>8)|(((A) & 0x00ff)<<8))//高低字节转换
	int client_sockfd;  
	int len;  
	struct sockaddr_in remote_addr; //服务器端网络地址结构体  
	uint16_t* IDsend;  //数据传送的缓冲区
	
	 typedef struct Buf
	 {
		 uint16_t ID;
		 uint16_t ID_SE;
		 unsigned char msg[1024];
	}buf;
	
/*发送线程函数*/	
void *func1(void *arg)
{
	buf msgto;
	msgto.ID=ID_VALUE;
	msgto.ID_SE =sizeof(buf);
	printf("Enter string to send:\n");
	scanf("%s", msgto.msg);
	printf("ID:%#x\n", msgto.ID);
	printf("ID字节数= %d\n", sizeof(msgto.ID));
	printf("ID字节数= %d\n", msgto.ID_SE);
	len = send(client_sockfd, (char *)&msgto, msgto.ID_SE, 0); //发送给服务器
	
}  

/*接受线程函数*/
void *func2(void *arg)
{
	IDsend = (uint16_t *)malloc(sizeof(uint16_t) * 2);
	len=recv(client_sockfd,IDsend,BUFSIZ,0);//接受来自服务器的消息  
	printf("received:%d\n",IDsend[1]); 
}  

int main(int argc, char *argv[])  
{  
	pthread_t tid1,tid2;
   
	memset(&remote_addr,0,sizeof(remote_addr)); //数据初始化--清零  
	remote_addr.sin_family=AF_INET; //设置为IP通信  
	remote_addr.sin_addr.s_addr=inet_addr("192.168.0.193");//服务器IP地址  
	remote_addr.sin_port=htons(PORT); //服务器端口号  
     
    /*创建客户端套接字--IPv4协议，面向连接通信，TCP协议*/  
    if((client_sockfd=socket(PF_INET,SOCK_STREAM,0))<0)  
    {  
        perror("socket error");  
        return 1;  
    } 
	
	// 4.等待链接请求并处理--》阻塞
	socklen_t len = sizeof(remote_addr);
	int ret = connect(client_sockfd, (struct sockaddr *)&remote_addr, len);
	if(ret < 0)
	{
		perror("connect");
		exit(1);
	}
	printf("Connection server successful \n");

    /*循环的发送接收信息并打印接收信息（可以按需发送）--recv返回接收到的字节数，send返回发送的字节数*/  
    while(1)  
    {  
		pthread_create(&tid1,NULL,func1,NULL);//创建线程
		pthread_create(&tid2,NULL,func2,NULL);//创建线程
		pthread_join(tid1,NULL);//接合指定线程（先执行线程）
    }  
      
    /*关闭套接字*/  
    close(client_sockfd);  
    return 0;  
}  
