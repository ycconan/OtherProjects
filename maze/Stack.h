/*-- Stack.h --------------------------------------------------------------

  这个头文件定义了一个 Stack 类
  基本操作：
    构造函数：    创建一个空栈
    empty:        检查栈是否为空
    push:         通过在栈顶添加一个元素来修改栈
    top:          取出栈顶元素，并且保持栈不被修改
    pop:          通过删除栈顶的元素来修改栈
    display:      显示栈中所有元素
    clear:        清空栈
  Note: Execution terminates if memory isn't available for a stack element.
--------------------------------------------------------------------------*/

#include <iostream>
using namespace std;

#ifndef STACK_H
#define STACK_H

#include "typedef.h"

class Stack
{
public:
    /***** Function Members *****/
    /***** Constructors *****/
    Stack();
    Stack(const Stack &original);
    //-- 赋值构造函数

    /***** Destructor *****/
    ~Stack();

    /***** Assignment *****/
    const Stack &operator= (const Stack &rightHandSide);
    bool empty() const;
    void push(const ElemType &value);
    //void display(ostream &out) const;
    ElemType top() const;
    void pop();
    void clear();

private:
    /*** Node class ***/
    class Node
    {
    public:
        ElemType data;
        Node *next;
        //--- Node constructor
        Node(ElemType value, Node *link = 0)
            : data(value), next(link)
        {}
    };

    typedef Node *NodePointer;

    /***** Data Members *****/
    NodePointer myTop;      // pointer to top of stack

}; // end of class declaration

#endif // STACK_H