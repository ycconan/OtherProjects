 /*-- main.cpp ---------------------------------*/
 
#include <iostream>
#include <cassert>
#include "Maze.h"

using namespace std;

int main()
{
    Maze maze;

    maze.init();
    maze.display();
    maze.getPath();
    maze.display();

    return 0;
}