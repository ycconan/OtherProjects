/*-- Maze.h --------------------------------------------------------------
  这个头文件一个 Maze 类

  成员函数：
    init          初始化迷宫
    getPath:     迷宫求解
    display:     输出迷宫
    pass:        判断该点是否可以通过
--------------------------------------------------------------------------*/
#ifndef MAZE_H
#define MAZE_H

#include <iostream>
#include <string>
#include "Stack.h"

using namespace std;


class Maze
{
public:
    /** Default constructor */
    Maze();

    void init();
    void display() const;
    void getPath(); // 迷宫求解
	
	
    /** Default destructor */
    virtual ~Maze();

private:
    bool pass(PosType);
    ElemType getNext(const PosType curPos); // 根据当前坐标寻找下一位置。

protected:

private:
    int r;
    int c;
    int **m;
    PosType entra;
    PosType exi;
    Stack s;

};

#endif // MAZE_H