 /*-- typedef.h --------------------------------------------------------------
  这个头文件定义了程序所用的一些数据类型

  数据类型：
    PosType：     迷宫中的位置
    Direction:    走步的方向
    ElemType:     栈元素类型
--------------------------------------------------------------------------*/

#ifndef TYPEDEF_H_INCLUDED
#define TYPEDEF_H_INCLUDED

typedef struct
{
    int r;
    int c;
} PosType;  // loctation in the maze

enum Direction
{
    NORTH = 2,
    WEST = 4,
    EAST = 6,
    SOUTH = 8
};

struct ElemType
{
    int step;
    PosType seat;
    int direction;

    ElemType(int s=1, int r=1, int c=1, int d=2)
    {
        step = s;
        seat.r = r;
        seat.c = c;
        direction = d;
    }
};

#endif // TYPEDEF_H_INCLUDED