#include<iostream>
using namespace std;
class A
{
  public:
    virtual void get() = 0;
    virtual void foo()
    {
        cout << "A::foo() is called" << endl;
    }
};
class B : public A
{
  public:
    void foo()
    {
        cout << "B::foo() is called" << endl;
    }
    void get()
    {
        cout << "B::get() is B" << endl;
    }
};
class C : public A
{
    public:
      void get()
      {
          cout << "C::get() is C" << endl;
      }
};
int main(void)
{
    A *a = new B();
    A *a1 = new C();
    a1->get(); 
    a1->foo();
    a->get();
    a->foo(); // 在这里，a虽然是指向A的指针，但是被调用的函数(foo)却是B的!
    return 0;
}