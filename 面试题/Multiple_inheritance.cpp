#include <iostream>
#include <string>

using namespace std;

class Furniture
{
  public:
    Furniture(void) : m_weight(0) {}
    Furniture(double weight) : m_weight(weight) {}
    ~Furniture(void) {}

    double GetWeight() const { return m_weight; }
    void SetWeight(double val) { m_weight = val; }

  private:
    double m_weight;
};

class Bed : virtual public Furniture
{
  public:
    Bed() : Furniture(), m_second(0) {}
    Bed(double weight, int second) : Furniture(weight), m_second(second) {}

    void Sleep(int second)
    {
        m_second = second;
        cout << "休息" << m_second << "秒..." << endl;
    }

  private:
    int m_second;
};

class Sofa : virtual public Furniture
{
  public:
    Sofa() : Furniture() {}
    Sofa(double weight) : Furniture(weight) {}

    void WatchTV(string programme)
    {
        cout << "正在看" << programme << "节目..." << endl;
    }
};

class SleepSofa : public Sofa,public Bed
{
  public:

    SleepSofa() : Bed(), Sofa() {}
    SleepSofa(double weight, int second) : Bed(weight, second), Sofa(weight) {}

    void FoldOut()
    {
        cout << "展开沙发当床用." << endl;
        Sleep(360);
    }
};

int main()
{
    SleepSofa sleepSofa;
    sleepSofa.Bed::SetWeight(95);
    sleepSofa.Sofa::SetWeight(90);
    cout<<sleepSofa.GetWeight()<<endl;
    double weight = sleepSofa.GetWeight();
    cout<<"weight="<<weight<<endl;

    return 0;
}