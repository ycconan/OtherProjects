/*获取一个字节的数字中有几位是1*/

#include <iostream>
#include <stdio.h>
#include <stdint.h>

int HaveNumOne(const uint8_t & c)
{
	int n = 0;
	for(int i = 0; i < 8; i++)
	{
		if(c & (1 << i))
			n++;
		
	}
	return n;
}

int main(void)
{	uint8_t a;
	printf("请输入：\n");
	scanf("%u", &a);
	printf("字符有 %d 个1\n",HaveNumOne(a));

return 0;
}
