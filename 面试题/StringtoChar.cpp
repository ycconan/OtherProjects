#include <cstring>
#include <iostream>

using namespace std;

/*string是一个字符串类，内部维护者一个字符串，封装了一些字符串处理函数*/
int main()
{
    char c[20];
    string s = "1213";
    strcpy(c, s.c_str());
    cout << "char c[20]:" << c << endl;

    string s1 = "abc";
    string s2 = "abd";
    if (s1 < s2) 
        cout << "string 类型 s1 = abc s21 = abd 直接比较   s1小于s2" << endl<< endl;
    if (strcmp(s1.c_str(), s2.c_str()) < 0) 
        cout << "string 类型 s1 = abc s2 = abd 用c_str()转换成char*后strcmp比较 s1小于s2" << endl << endl;
        
    cout << "char* 数组比较" << endl<< endl;
    char s3[10] = "abc";
    char s4[10] = "def";
    if (strcmp(s3, s4) < 0)
        cout << "strcmp比较  s3小于s4" << endl << endl;
    if (s3 > s4) 
        cout << "直接比较s3大于s4，明显是按照地址先后进行比较的";

    return 0;
}
