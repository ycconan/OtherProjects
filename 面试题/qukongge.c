
#include <stdio.h>  
/** 
 *  删除字符串空格 
 */  
void delSpace(char *str)  
{  
    char *p = str;  
    char *t =p;  
    while (*p != '\0') {  
        if (*p == ' ') {  
            t++;  
            if (*t != ' ') {  
                *p = *t;  
                *t = ' ';  
            }  
        }else  
        {  
            p++;  
            t = p;  
        }  
    }  
}  
int main(int argc, const char * argv[]) {  
    char str[] = "abcdefgh";  
    delSpace(str);  
    printf("%s\n",str);  
    return 0;  
}  