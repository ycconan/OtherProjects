#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include <time.h>
#include <stdint.h>
#include <algorithm>

#define SETC_COLPOI(c, p) ((p << 4) + c)
#define GETC_COLOR(c) (c & 0xf)
#define GETC_POINT(p) ((p >> 4) & 0xf)
#define GETC_REALPOINT(p) (((p >> 4) & 0xf) > 10 ? 10 : (p)&0xf)

using namespace std;

int main()
{
    uint8_t point = 1;
    uint8_t color = 1;
    uint16_t num = SETC_COLPOI(color, point);

    cout<<"num= "<<num<<"\t";
    cout << "point= " <<   GETC_COLOR(num)<< " color= " <<  GETC_POINT(num);

    return 0;
}
