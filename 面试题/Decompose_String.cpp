/*输入两个数M，N；M代表输入的M串字符串，N代表输出的每串字符串的位数，不够补0*/
#include<iostream>
#include<cstdio>
#include<cstring>

using namespace std;
void solve(char *str,int n,int len)
{
    int i, j, k, DecomNum, SurplusNum;
    DecomNum = len / n;
    SurplusNum = len - n*DecomNum;

    for(i=0;i<len;i+=n)
    {
        if(len-i<n)
        {
            k = n - (len-i);
            for(j = i; j < len; ++j)
            printf("%c",str[j]);
            for(j = 0; j < k; ++j)
            putchar('0');
        }
        else
        {
            for(j = i; j < i; ++j)
                printf("%c",str[j]);
        }
        putchar(' ');

    }
    printf("\n");
}

int main()
{
    int i,m,n,len;
    char str[1000];

    while(scanf("%d %d",&m,&n)!=EOF)
    {
        for(i=0;i<m;++i)
        {
            scanf("%s",str);
            len = strlen(str);
            solve(str,n,len);
        }
    }

    return 0;
}