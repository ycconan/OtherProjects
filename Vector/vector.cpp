#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> iVec;

    for(int i=1;i<=33;i++)
    {
        iVec.push_back(i);
        cout << "容器 大小为: " << iVec.size() << endl;
        cout << "容器 容量为: " << iVec.capacity() << endl; 
        cout << endl;
    }
    /* linux/2018/1/12 容量增长是翻倍的，如 
    9个元素   容量16 
    17个元素 容量32 */

    int  pos = iVec.at(2);
  iVec.erase(iVec.begin()+2);
  iVec.insert(iVec.begin()+iVec.size(),pos);
  cout<<"交换顺序之后的："<<endl;
  for (int i = 0; i < 33; i++)
  {
      cout << iVec[i] << endl; /*将d的地址压入容器c中，即c[0]=d；因此释放了b的地址也就是释放了a[0]的地址，结果不得而知*/
  }
  /* 测试effective stl中的特殊的交换 swap() */
  cout << "当前vector 的大小为: " << iVec.size() << endl;
  cout << "当前vector 的容量为: " << iVec.capacity() << endl;
  cout << endl;
  vector<int>(iVec).swap(iVec);

  cout << "临时的vector<int>对象 的大小为: " << (vector<int>(iVec)).size() << endl;
  cout << "临时的vector<int>对象 的容量为: " << (vector<int>(iVec)).capacity() << endl;
  cout << endl;
  cout << "交换后，当前vector 的大小为: " << iVec.size() << endl;
  cout << "交换后，当前vector 的容量为: " << iVec.capacity() << endl;

  vector<int> a;
  int b = 5;
  a.push_back(b);
  cout << "加入b元素：" << a[0] << endl; /*往容器中压入的是b的值，a[0]与b存储在不同的地址中，改变b的值不会改变容器中a[0]的值*/

  vector<int *> c;
  int *d;
  d = new int[4];
  d[0] = 0;
  d[1] = 1;
  d[2] = 2;
  c.push_back(d);
  delete d; //释放b的地址空间
  for (int i = 0; i < 3; i++)
  {
      cout << c[0][i] << endl; /*将d的地址压入容器c中，即c[0]=d；因此释放了b的地址也就是释放了a[0]的地址，结果不得而知*/
    }
    
    vector <string> e;
    char ch;

    for (int i = 0; i < 100000; i++)
        e.push_back("abcdefghijklmn");
    cin >> ch;
    // 此时检查内存情况 占用54M
    e.clear();
    cin >> ch;
    // 此时再次检查， 仍然占用54M

    cout << "Vector 的 容量为" << e.capacity() << endl;
    // 此时容量为 104857
    vector<string>(e).swap(e);//销毁释放容器方式
    cout << "Vector 的 容量为" << e.capacity() << endl;
    // 此时容量为0
    cin >> ch;
    // 检查内存，释放了 10M+ 即为数据内存

    return 0;
}
