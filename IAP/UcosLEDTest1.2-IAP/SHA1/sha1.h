/****************************************************************************
* @file     sha1.h
* @brief    SHA1加密算法
* @version  V1.20
* @date     2018/5/25
***************************************************************************/

#ifndef _SHA1_H
#define _SHA1_H
#include <stdio.h>
#include <string.h> 

#define SHA1_DIGEST_LENGTH 20

typedef  unsigned int uint32_t;
typedef  unsigned char uint8_t;

typedef struct {
	uint32_t h[5];
	uint32_t count_lo, count_hi;
	uint8_t buf[64];
} tpm_sha1_ctx_t;

//错误码，参数空间不满足要求
#define UNSATISFACTORY 303

/**
* @brief  初始化上下文
* @param  ctx: tpm_sha1_ctx_t类型的地址
* @retval None
*/
void tpm_sha1_init(tpm_sha1_ctx_t *ctx);

/**
* @brief  载入待处理文本
* @param  ctx: tpm_sha1_ctx_t类型的地址
* @param  data: 待操作数据
* @param  length: 待操作数据长度
* @retval None
* @Note	  该函数可反复调用，讲一个长句分割为不同短句分别hash
*/
void tpm_sha1_update(tpm_sha1_ctx_t *ctx, const uint8_t *data, size_t length);

/**
* @brief  返回sha1结果
* @param  ctx: tpm_sha1_ctx_t类型的地址
* @param  digest: 返回结果容器
* @retval None
*/
void tpm_sha1_final(tpm_sha1_ctx_t *ctx, uint8_t digest[SHA1_DIGEST_LENGTH]);

/**
* @brief  快速sha1加密
* @param  data: 待sha1的数据
* @param  length: 待sha1的数据长度
* @param  digest: 返回结果的容器
* @param  digest: 返回结果的容器
* @retval None
*/
void sha1(const uint8_t *data, size_t length, uint8_t digest[SHA1_DIGEST_LENGTH]);

/**
* @brief  利用printf输出sha结果
* @param  data: sha1后的数据
* @retval None
*/
void printsha1(uint8_t digest[SHA1_DIGEST_LENGTH]);

/**
* @brief  hex转hexString
* @param  str:hex进制数据
* @param  dst:转化后的hexString
* @param  len:hex长度
* @retval dst
*/
char* hex2String(uint8_t str[SHA1_DIGEST_LENGTH], char dst[SHA1_DIGEST_LENGTH * 2 + 1], int len);

#endif // _SHA1_H
