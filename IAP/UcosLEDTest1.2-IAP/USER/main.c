#include "sys.h"
#include "delay.h"
#include "led.h"
#include "key.h"
#include "beep.h"
#include "timer.h"
#include "flash.h"
#include "stmflash.h"
#include "usart4.h"
#include "LoopList.h"
#include "malloc.h"
#include "sha1.h"

void CLearUserInfo(void)
{
	__IO u32 UserDataAddr = 0;
	u16 ReadDat16=0;//读数据间接变量
	
	UserDataAddr = STMFLASH_DATA_ADDR;//存储其余的数据地址
	ReadDat16 = 0x00;//写入标志告诉IAP程序有可更新的用户程序
	STMFLASH_Write(UserDataAddr,&ReadDat16,1);
	UserDataAddr+=2;
	
	STMFLASH_Write(UserDataAddr,&ReadDat16,1);
	UserDataAddr+=2;

	STMFLASH_Write(UserDataAddr,&ReadDat16,1);
}

u8 printSHA(int UpdaCnt, uint8_t *check)
{
	__IO u32 addr=FLASH_APP2_ADDR;
	
	u32 i;
	tpm_sha1_ctx_t ctx;
	
	u8 buff[1024];
	uint8_t digest[SHA1_DIGEST_LENGTH];
	
	tpm_sha1_init(&ctx);
	
	for(i=0;i<UpdaCnt; i+=1024)
	{
		if(i+1024<UpdaCnt)
		{	
			SPI_Flash_Read(buff, addr, 1024);
			tpm_sha1_update(&ctx, buff, 1024);
			addr+=1024;
		}
		else
		{
			SPI_Flash_Read(buff, addr, UpdaCnt-i);
			tpm_sha1_update(&ctx, buff, UpdaCnt-i);
			addr+=(UpdaCnt-i);	
		}
	}
	
	printf("直接读取内存校验....\r\n");
	tpm_sha1_final(&ctx, digest);//存储sha结果
	printsha1(digest);
	return memcmp((char*)digest, (char*)check, SHA1_DIGEST_LENGTH);
}

int main()
{
	__IO u32 addr1 = 0;
	__IO u32 addr2 = 0;
	__IO u32 UserDataAddr = 0;
	__IO u32 UpdaCnt=0;//程序的大小
	
	u8* buff;
	u8 UpdaFlage = 0;//是否有更新程序
	u16 TickCnt = 0;
	u16 ReadDat16=0;//读数据间接变量
	u16 TickOUT = 50;
	u32 i;
	
	tpm_sha1_ctx_t ctx;
	uint8_t digest[SHA1_DIGEST_LENGTH];
	uint8_t progarm[SHA1_DIGEST_LENGTH];
	
	NVIC_Configuration();
	sys_srand(1);
	my_mem_init(SRAMIN);
	uart_init(115200);
	usart4_init(57600);
	delay_init();
	led_init();
	key_init();
	beep_init();
	SPI_Flash_Init();
	
	addr1 = STMFLASH_APP1_ADDR;
	addr2 = FLASH_APP2_ADDR;
	UserDataAddr = STMFLASH_DATA_ADDR;
	buff = mymalloc(SRAMIN, 1024);
		
	if(KEY_Scan(0) != 0)//按键强制删除代码
	{
		CLearUserInfo();
		for(i=0;i<255;i++)
		{
			STMFLASH_Erase(addr1,1024);//擦除FLASH_APP1_ADDR地址以及以上40页
			addr1 +=2048;
		}
	}
	
	STMFLASH_Read(UserDataAddr, &ReadDat16, 1);
	if(ReadDat16 == 0x5050)
	{
		UpdaFlage = 1;
		UserDataAddr += 2;
		STMFLASH_Read(UserDataAddr, &ReadDat16, 1);//读取字节数
		UpdaCnt = (u32)ReadDat16 << 16;
		UserDataAddr += 2;
		STMFLASH_Read(UserDataAddr, &ReadDat16, 1);
		UserDataAddr += 2;
		UpdaCnt = UpdaCnt | (u32)ReadDat16;
		if(UpdaCnt == 0)
		{
			CLearUserInfo();
			Reset_MCU();
		}
		
		STMFLASH_Read(UserDataAddr, (u16*)progarm, SHA1_DIGEST_LENGTH/2);
		
		printf("UpdaCnt:%d\r\n", UpdaCnt);
		
		//内存校验
		if(printSHA(UpdaCnt, progarm) != 0)
		{
			printf("程序接收校验失败\r\n");
	    addr2 = FLASH_APP2_ADDR;//存储用户程序地址
			SPI_Flash_Erase_Sector(addr2);//擦除addr2地址
			CLearUserInfo();
			Reset_MCU();
		}
	}
	else
  {
		printf("擦除存储用户程序地址Flash\r\n");
    SPI_Flash_Erase_Sector(addr2);//擦除addr2地址
  }	

	printf("开始进入主函数\r\n");
	while(1)
	{
		if(UpdaFlage)
		{
      UpdaFlage = 0;	
			addr1 = STMFLASH_APP1_ADDR;//APP程序地址
			for(i=0;i<255;i++)
			{
        STMFLASH_Erase(addr1,1024);//擦除FLASH_APP1_ADDR地址以及以上40页
				addr1 +=2048;
      }
			printf("擦除了FLASH_APP1_ADDR....\r\n");
			
			addr1 = STMFLASH_APP1_ADDR;//APP程序地址
	    addr2 = FLASH_APP2_ADDR;//存储用户程序地址
			
			printf("正在拷贝程序....\r\n");
			for(i=0;i<UpdaCnt/2;i+=512)
			{
				if(i+512<UpdaCnt/2)
				{	
					SPI_Flash_Read(buff, addr2, 1024);
					STMFLASH_Write(addr1,(u16*)buff, 512);
					addr1+=1024;
					addr2+=1024;			
				}
				else
				{
					SPI_Flash_Read(buff, addr2, UpdaCnt-2*i);
					STMFLASH_Write(addr1,(u16*)buff, UpdaCnt/2-i);
					addr1+=(UpdaCnt-2*i);
					addr2+=(UpdaCnt-2*i);			
				}
				printf("%d/%d\r\n", 2*i,UpdaCnt);
			}
			printf("程序已经拷贝....\r\n");
			printf("擦除存储用户程序地址Flash\r\n");
			addr2 = FLASH_APP2_ADDR;//存储用户程序地址
			SPI_Flash_Erase_Sector(addr2);//擦除addr2地址
			CLearUserInfo();
			
			if(((*(vu32*)(STMFLASH_APP1_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
		  {	 
			  printf("准备执行新的APP代码!!\r\n");
			  iap_load_app(STMFLASH_APP1_ADDR);//执行FLASH APP代码
		  }
		  else 
		  {
			  printf("拷贝的程序非FLASH应用程序,无法执行!RstMCU\r\n");
				Reset_MCU();//复位重启CPU
		  }		
		}
		else
		{
			if(((*(vu32*)(STMFLASH_APP1_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
		  {	 
			  printf("准备执行自带的APP代码!!\r\n");
			  iap_load_app(STMFLASH_APP1_ADDR);//执行FLASH APP代码
		  }
		  else 
		  {
				if(TickCnt >= 1000)
				{
					TickCnt = 0;
					LED0 = ~LED0;
					printf("等待接收用户程序!!!!\r\n");
					UpdaCnt = 0;
				}
		  }
			
			if(i = rbCanRead(&pRb), i)//数据读取，同时进行SHA
			{
				if(UpdaCnt == 0)
				{
					tpm_sha1_init(&ctx);
				}
				
				printf("RECV:%d %d\r\n", UpdaCnt,i);
				
				i=rbRead(&pRb, buff, i);
				tpm_sha1_update(&ctx, buff, i);
				SPI_Flash_Write(buff, addr2, i);
				TickCnt = 0;
				addr2 += i;
				UpdaCnt += i;//接收计数
			}
			
			if(TickCnt >= 500 && UpdaCnt)//接收完成
			{
				TickCnt = 0;
				UserDataAddr = STMFLASH_DATA_ADDR;//存储其余的数据地址
				printf(">RECV:%d/%d<\r\n", UpdaCnt, USARTCONT);
				
				if(Overflow==1)//如果中途溢出了
				{
					printf("程序中途溢出,准备复位重启!!\r\n");
					CLearUserInfo();
					Reset_MCU();//复位重启CPU
				}
				
				printf("准备执行APP代码!!\r\n");
				
				ReadDat16 = 0x5050;//写入标志告诉IAP程序有可更新的用户程序
				STMFLASH_Write(UserDataAddr,&ReadDat16,1);
				UserDataAddr+=2;
				
				ReadDat16 = (u16)((UpdaCnt>>16)&0xffff);//存储接收到多少数据高位
				STMFLASH_Write(UserDataAddr,&ReadDat16,1);
				UserDataAddr+=2;
				
				ReadDat16 = (u16)(UpdaCnt&0xffff);//存储接收到多少数据低位
				STMFLASH_Write(UserDataAddr,&ReadDat16,1);
				UpdaCnt = 0;
				
				UserDataAddr+=2;
				tpm_sha1_final(&ctx, digest);//存储sha结果
				STMFLASH_Write(UserDataAddr, (u16*)digest, SHA1_DIGEST_LENGTH/2);
				printf("接收校验\r\n");
				printsha1(digest);
				
				printf("开始复位重启!!\r\n");
				Reset_MCU();
			}
			delay_ms(1);
			TickCnt++;
		}
	}
}
