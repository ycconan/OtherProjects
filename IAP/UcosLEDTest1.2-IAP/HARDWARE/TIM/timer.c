#include "timer.h"
#include "led.h"

u8 TIM3_Flag;

//定时时间为72M/PSC/ARR
void TIM3_Int_Init(u16 arr,u16 psc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); 

	TIM_TimeBaseStructure.TIM_Period = arr-1;
	TIM_TimeBaseStructure.TIM_Prescaler =psc-1; 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
 
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);

	TIM_Cmd(TIM3, ENABLE);		
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure); 
	TIM3_Flag = 0;
}


//定时器处理函数模板
void TIM3_IRQHandler(void) 
{
#if SYSTEM_SUPPORT_OS
	OSIntEnter();
#endif
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		TIM3_Flag = 1;
		printf("TIM3_IRQHandler\r\n");
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	}
#if SYSTEM_SUPPORT_OS
	OSIntExit();
#endif
}
