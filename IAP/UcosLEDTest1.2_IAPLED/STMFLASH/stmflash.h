#ifndef __STMFLASH_H__
#define __STMFLASH_H__
#include <stm32f10x.h>

//用户根据自己的需要设置
#define STM32_FLASH_SIZE 512 	 		//所选STM32的FLASH容量大小(单位为K)
#define STM32_FLASH_WREN 1        //使能FLASH写入(0，不是能;1，使能)
//FLASH起始地址
#define STM32_FLASH_BASE			0x08000000 	//STM32 FLASH的起始地址

//IAP相关配置
#define STMFLASH_APP1_ADDR   	0x08010000  //用户程序的执行地址

#define FLASH_APP2_ADDR   		0x00780000  //用户程序的备份地址,W25Q64的最后一个扇区512KB

#define STMFLASH_DATA_ADDR		0x0800f000  //存储用户数据的地址


u16 STMFLASH_ReadHalfWord(u32 faddr);		  //读出半字  
void STMFLASH_WriteLenByte(u32 WriteAddr,u32 DataToWrite,u16 Len);	//指定地址开始写入指定长度的数据
u32 STMFLASH_ReadLenByte(u32 ReadAddr,u16 Len);						//指定地址开始读取指定长度数据
void STMFLASH_Write(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite);		//从指定地址开始写入指定长度的数据
void STMFLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead);   		//从指定地址开始读出指定长度的数据
void STMFLASH_Erase(u32 WriteAddr,u16 NumToWrite);

//测试写入
void Test_Write(u32 WriteAddr,u16 WriteData);								   
#endif
