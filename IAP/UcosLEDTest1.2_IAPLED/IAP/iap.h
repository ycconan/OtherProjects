#ifndef __IAP_H__
#define __IAP_H__

#include "sys.h"
#include "sha1.h"
#include "malloc.h"
#include "flash.h"
#include "stmflash.h"

#define INVALIDVAL	0xFFFF

typedef  void (*iapfun)(void);
typedef u16 (*IAP_RecvFun)(u8* data, u16 rlen, s16 waittime);

void IAP_JumpExec(void);
void IAP_RecvFile(IAP_RecvFun FunRecv, u16 byte);

#endif
