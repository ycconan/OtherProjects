#ifndef __KEY_H
#define __KEY_H

#include "sys.h"
#include "delay.h"
/*
#define KEY0  PCin(3)//读取按键KEY0
#define KEY1  PCin(2)//读取按键KEY1
#define KEY2  PCin(1)//读取按键KEY2
#define KEY3  PAin(1)//读取按键WK_UP
*/

#define KEY0  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_3)//读取按键KEY0
#define KEY1  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_2)//读取按键KEY1
#define KEY2  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_1)//读取按键KEY2
#define KEY3  GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_1)//读取按键WK_UP

#define KEY0_VAL 1
#define KEY1_VAL 2
#define KEY2_VAL 3
#define KEY3_VAL 4

void key_init(void);

//按键扫描
//mode:1,支持连按；mode:0，不支持连按
u8 KEY_Scan(u8 mode);
#endif

