#include<iostream>
#include <algorithm>
using namespace std;

int main()
{
 int a, b, c;
 a = b = c = 0;
 //在fun1中a、b、c都采用值捕获
 auto fun1 = [=]()mutable{
  a++;
  b++;
  c++;
 };
 //在fun2中a、b、c都采用引用捕获
 auto fun2 = [&](){
  a++;
  b++;
  c++;
 };
 //在fun3中a采用引用捕获，b、c采用值捕获
 auto fun3 = [=, &a](){
  a++;
 };
 //在fun4中b、c采用引用捕获，a采用值捕获
 auto fun4 = [&, a](){
  b++;
  c++;
 };
 fun1();
 cout << "fun1() a = " << a << " b = " << b << " c = " << c << endl;
 a = b = c = 0;
 fun2();
 cout << "fun2() a = " << a << " b = " << b << " c = " << c << endl;
 a = b = c = 0;
 fun3();
 cout << "fun3() a = " << a << " b = " << b << " c = " << c << endl;
 a = b = c = 0;
 fun4();
 cout << "fun4() a = " << a << " b = " << b << " c = " << c << endl;
 cin.get();
 return 0;
}